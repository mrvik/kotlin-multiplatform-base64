plugins {
    kotlin("multiplatform") version "1.6.20"
    id("maven-publish")
}

group = "tk.mrvik"
version = System.getenv("CI_COMMIT_TAG").let {
    if (it?.isNotBlank() == false) it
    else "0.0.0-SNAPSHOT"
}

val gitlabAPIURL = System.getenv("CI_API_V4_URL") ?: "https://gitlab.com/api/v4"

repositories {
    mavenCentral()
}

publishing {
    repositories {
        maven {
            name = "GitLab"
            url = uri("${gitlabAPIURL}/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("Job-Token", HttpHeaderAuthentication::class)
            }
        }
    }
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }

    js(BOTH) {
        nodejs {}
        browser {
            commonWebpackConfig {
                cssSupport.enabled = true
            }
        }
    }

    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS ($hostOs) is not supported in Kotlin/Native.")
    }

    sourceSets {
        // Common source set in pure-kotlin, including a reference implementation.
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        // JVM implementation
        val jvmMain by getting
        val jvmTest by getting

        // NodeJS target
        val jsMain by getting {
            dependencies {
                implementation(npm("buffer", "6.0.3"))
            }
        }
        val jsTest by getting

        // Native target to expose the pure-kotlin implementation
        val nativeMain by getting
        val nativeTest by getting
    }
}