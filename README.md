# Kotlin/Multiplatform base64 implementation

It comes with support for:

* Native (pure Kotlin port of Go's `encoding/base64` package)
* JS (using `js-base64` npm library)
* JVM (using `java.util.Base64`)

Tests are in common, so all implementations behave the same way (also when it comes to handling `utf8`).