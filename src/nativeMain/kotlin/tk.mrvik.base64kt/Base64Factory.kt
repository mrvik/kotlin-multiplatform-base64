package tk.mrvik.base64kt

actual object Base64Factory {
    actual fun createEncoder(encoding: Encoding, padding: Char?): IBase64Encoder = EncoderImpl(encoding, padding)
    actual fun createDecoder(encoding: Encoding, padding: Char?): IBase64Decoder = DecoderImpl(encoding, padding)
}