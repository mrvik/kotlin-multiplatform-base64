package tk.mrvik.base64kt

import java.util.*

class Base64Decoder(encoding: Encoding, padding: Char?) : IBase64Decoder {
    private val decoder = (when (encoding) {
        Encoding.URL -> Base64.getUrlDecoder()
        Encoding.Standard -> Base64.getDecoder()
        else -> throw Error("Only standard and URL decoders are allowed in jvm")
    }).let {
        when (padding) {
            EncoderImpl.StdPadding, EncoderImpl.NoPadding -> it
            else -> throw Error("No paddings other than the standard and none are allowed")
        }
    }

    override fun decode(bytes: ByteArray): ByteArray = decoder.decode(bytes)

    override fun decodeString(str: String): ByteArray = decoder.decode(str)

    override fun decodeToString(str: String): String = decodeString(str).decodeToString()
}