package tk.mrvik.base64kt

import java.util.*

class Base64Encoder(encoding: Encoding, padding: Char?) : IBase64Encoder {
    private val encoder = (when (encoding) {
        Encoding.URL -> Base64.getUrlEncoder()
        Encoding.Standard -> Base64.getEncoder()
        else -> throw Error("Only URL and Standard encodings allowed")
    }).let {
        when (padding) {
            EncoderImpl.StdPadding -> it
            EncoderImpl.NoPadding -> it.withoutPadding()
            else -> throw Error("Only null and standard paddings allowed")
        }
    }

    override fun encode(bytes: ByteArray): ByteArray {
        return encoder.encode(bytes)
    }

    override fun encodeToString(bytes: ByteArray): String {
        return encoder.encodeToString(bytes)
    }

    override fun encodeString(str: String): String {
        return encoder.encodeToString(str.encodeToByteArray())
    }
}