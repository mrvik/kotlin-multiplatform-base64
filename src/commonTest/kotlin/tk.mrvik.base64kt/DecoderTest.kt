package tk.mrvik.base64kt

import kotlin.test.Test

class DecoderTest {
    private val decoders = listOf(
        Pair(
            Base64Factory.createDecoder(encoding = Encoding.Standard),
            "VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA==",
        ),
        Pair(
            Base64Factory.createDecoder(encoding = Encoding.Standard, padding = EncoderImpl.NoPadding),
            "VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA",
        ),
        Pair(
            Base64Factory.createDecoder(encoding = Encoding.URL),
            "VGVzdCDDscOhw6nDpyA_P2Jhc2U2NA==",
        ),
        Pair(
            Base64Factory.createDecoder(encoding = Encoding.URL, padding = EncoderImpl.NoPadding),
            "VGVzdCDDscOhw6nDpyA_P2Jhc2U2NA",
        ),
        Pair(
            DecoderImpl(encoding = Encoding.Standard, padding = EncoderImpl.StdPadding),
            "VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA==",
        ),
    )

    private val testingBytes = "Test ñáéç ??base64".encodeToByteArray()

    private val decodeDatasets = decoders.map {
        DecoderTestHelper(testingBytes, it.second.encodeToByteArray(), it.first)
    }

    @Test
    fun decode() {
        decodeDatasets.forEach(DecoderTestHelper::test)
    }
}