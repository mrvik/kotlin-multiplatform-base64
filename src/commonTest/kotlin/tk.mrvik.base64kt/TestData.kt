package tk.mrvik.base64kt

import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

fun interface Generator<T> {
    fun invoke(): T
}

interface IEQTester {
    fun testEquals(message: String)
}

open class TestData<T>(val expect: T, private val generate: Generator<T>) : IEQTester {
    protected var value: T? = null
        get() = if (field != null) field else generate.invoke().also { value = it }

    override fun testEquals(message: String) {
        assertEquals(expect, value, message)
    }
}

class TestDataCE(expect: ByteArray, generate: Generator<ByteArray>) : TestData<ByteArray>(expect, generate) {
    override fun testEquals(message: String) {
        assertContentEquals(expect, value, message)
    }
}

data class EncoderTestHelper(val data: ByteArray, val expected: String, val encoder: IBase64Encoder) {
    private fun strTest(gen: Generator<String>): TestData<String> = TestData(expected, gen)

    fun test() {
        listOf<IEQTester>(
            TestDataCE(expected.encodeToByteArray()) { encoder.encode(data) },
            strTest { encoder.encodeString(data.decodeToString()) },
            strTest { encoder.encodeToString(data) },
        ).forEach {
            it.testEquals("Encode functions return the expected result")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as EncoderTestHelper

        if (!data.contentEquals(other.data)) return false
        if (expected != other.expected) return false
        if (encoder != other.encoder) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data.contentHashCode()
        result = 31 * result + expected.hashCode()
        result = 31 * result + encoder.hashCode()
        return result
    }
}

data class DecoderTestHelper(val expected: ByteArray, val encoded: ByteArray, val decoder: IBase64Decoder) {
    private fun bufTest(gen: Generator<ByteArray>): TestDataCE = TestDataCE(expected, gen)

    fun test() {
        listOf(
            Pair(
                bufTest { decoder.decode(encoded) },
                "Raw bytes decoding",
            ),
            Pair(
                bufTest { decoder.decodeString(encoded.decodeToString()) },
                "Decode string to raw bytes",
            ),
            Pair(
                TestData(expected.decodeToString()) { decoder.decodeToString(encoded.decodeToString()) },
                "Decode string to string",
            ),
        ).forEach {
            it.first.testEquals("Decode functions return the expected result (function=${it.second})")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as DecoderTestHelper

        if (!expected.contentEquals(other.expected)) return false
        if (!encoded.contentEquals(other.encoded)) return false
        if (decoder != other.decoder) return false

        return true
    }

    override fun hashCode(): Int {
        var result = expected.contentHashCode()
        result = 31 * result + encoded.contentHashCode()
        result = 31 * result + decoder.hashCode()
        return result
    }
}