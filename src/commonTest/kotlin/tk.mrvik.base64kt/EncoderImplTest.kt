package tk.mrvik.base64kt

import kotlin.test.Test
import kotlin.test.assertEquals

class EncoderImplTest : EncoderTest() {
    private val stdEncoderPad = EncoderImpl(encoding = Encoding.Standard)
    private val stdEncoderNoPad = EncoderImpl(encoding = Encoding.Standard, padding = EncoderImpl.NoPadding)

    @Test
    fun encodedLength() {
        assertEquals("VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA==".length, stdEncoderPad.encodedLength(testingBytes.size))
        assertEquals("VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA".length, stdEncoderNoPad.encodedLength(testingBytes.size))
    }
}