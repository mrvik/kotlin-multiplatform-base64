package tk.mrvik.base64kt
import kotlin.test.Test

open class EncoderTest {
    private val stdEncoderPad = Base64Factory.createEncoder(encoding = Encoding.Standard)
    private val stdEncoderNoPad =
        Base64Factory.createEncoder(encoding = Encoding.Standard, padding = EncoderImpl.NoPadding)
    private val urlEncoderPad = Base64Factory.createEncoder(encoding = Encoding.URL)
    private val urlEncoderNoPad = Base64Factory.createEncoder(encoding = Encoding.URL, padding = EncoderImpl.NoPadding)

    private val testingString = "Test ñáéç ??base64"
    protected val testingBytes = testingString.encodeToByteArray()

    private fun prepareDataset(expected: String, encoder: IBase64Encoder): EncoderTestHelper =
        EncoderTestHelper(testingBytes, expected, encoder)

    private val encodeDatasets = listOf(
        prepareDataset("VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA==", stdEncoderPad),
        prepareDataset("VGVzdCDDscOhw6nDpyA/P2Jhc2U2NA", stdEncoderNoPad),
        prepareDataset("VGVzdCDDscOhw6nDpyA_P2Jhc2U2NA==", urlEncoderPad),
        prepareDataset("VGVzdCDDscOhw6nDpyA_P2Jhc2U2NA", urlEncoderNoPad),
    )

    @Test
    fun encode() {
        encodeDatasets.forEach(EncoderTestHelper::test)
    }
}