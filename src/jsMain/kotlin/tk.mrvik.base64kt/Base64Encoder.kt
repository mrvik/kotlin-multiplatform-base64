package tk.mrvik.base64kt

import shared.AbsBase64Encoder
import shared.IJSBase64Adapter

class Base64Encoder(encoding: Encoding, padding: Char?) : AbsBase64Encoder(encoding, padding) {
    override val encoder: IJSBase64Adapter
        get() = Base64Adapter()
}