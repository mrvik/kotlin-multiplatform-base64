package tk.mrvik.base64kt

import shared.AbsBase64Decoder
import shared.IJSBase64Adapter

class Base64Decoder(encoding: Encoding) : AbsBase64Decoder(encoding) {
    override val adapter: IJSBase64Adapter
        get() = Base64Adapter()
}