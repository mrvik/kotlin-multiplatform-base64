package tk.mrvik.base64kt

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.ArrayBufferView
import org.khronos.webgl.Uint8Array
import shared.IJSBase64Adapter

internal interface Generator<T> {
    fun run(): T
}

class Base64Adapter : IJSBase64Adapter {
    private val acceptsURL =
        kotlin.runCatching {
            BufferModule.Buffer.from("testString", "utf8").toString("base64url")
        }.isSuccess

    private fun generateBuffer(u8s: Uint8Array): BufferModule.Buffer = BufferModule.Buffer.from(u8s)
    private fun generateBuffer(str: String): BufferModule.Buffer = BufferModule.Buffer.from(str, "utf8")

    private fun uriSafeEncode(it: BufferModule.Buffer, uriSafe: Boolean): String =
        if (acceptsURL || !uriSafe) it.toString("base64${if (acceptsURL && uriSafe) "url" else ""}")
        else makeURLSafe(it.toString("base64"))

    override fun fromUint8Array(u8s: Uint8Array, uriSafe: Boolean): String =
        uriSafeEncode(generateBuffer(u8s), uriSafe)

    override fun encode(str: String, uriSafe: Boolean): String =
        uriSafeEncode(generateBuffer(str), uriSafe)

    override fun decodeString(str: String, uriSafe: Boolean): String =
        BufferModule.Buffer.from(str, getEncoding(uriSafe)).toString("utf8")

    override fun decodeBytes(str: String, uriSafe: Boolean): Uint8Array =
        BufferModule.Buffer.from(str, getEncoding(uriSafe)).let {
            Uint8Array(it.buffer, it.byteOffset, it.byteLength / Uint8Array.BYTES_PER_ELEMENT)
        }

    private fun getEncoding(uriSafe: Boolean): String = "base64${if (acceptsURL && uriSafe) "url" else ""}"

    private fun makeURLSafe(converted: String): String = converted
        .map {
            when (it) {
                '/' -> '_'
                '+' -> '-'
                else -> it
            }
        }
        .joinToString("")
}

//@JsModule("buffer")
@JsModule("buffer")
@JsNonModule
@JsName("buffer")
external object BufferModule {
    class Buffer : ArrayBufferView {
        companion object {
            fun from(str: String, encoding: String = definedExternally): Buffer
            fun from(u8s: Uint8Array): Buffer
        }

        override val buffer: ArrayBuffer = definedExternally
        override val byteLength: Int = definedExternally
        override val byteOffset: Int = definedExternally

        fun copy(
            target: Uint8Array,
            targetStart: Int = definedExternally,
            sourceStart: Int = definedExternally,
            sourceEnd: Int = definedExternally,
        ): Int

        val length: Int = definedExternally

        fun toString(encoding: String = definedExternally): String
        override fun toString(): String
    }
}