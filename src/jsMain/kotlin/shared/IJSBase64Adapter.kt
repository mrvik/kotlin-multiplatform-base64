package shared

import org.khronos.webgl.Uint8Array

interface IJSBase64Adapter {
    fun fromUint8Array(u8s: Uint8Array, uriSafe: Boolean): String
    fun encode(str: String, uriSafe: Boolean): String
    fun decodeString(str: String, uriSafe: Boolean): String
    fun decodeBytes(str: String, uriSafe: Boolean): Uint8Array
}