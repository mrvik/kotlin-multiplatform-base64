package shared

import org.khronos.webgl.get
import tk.mrvik.base64kt.Encoding
import tk.mrvik.base64kt.IBase64Decoder

abstract class AbsBase64Decoder(encoding: Encoding) : IBase64Decoder {
    abstract val adapter: IJSBase64Adapter
    private val isUriSafe = encoding == Encoding.URL

    override fun decode(bytes: ByteArray): ByteArray = decodeString(bytes.decodeToString())

    override fun decodeString(str: String): ByteArray = adapter.decodeBytes(str, isUriSafe).let {
        ByteArray(it.length, it::get)
    }

    override fun decodeToString(str: String): String = adapter.decodeString(str, isUriSafe)
}