package shared

import org.khronos.webgl.Uint8Array
import tk.mrvik.base64kt.EncoderImpl
import tk.mrvik.base64kt.Encoding
import tk.mrvik.base64kt.IBase64Encoder

abstract class AbsBase64Encoder(private val encoding: Encoding, private val padding: Char?) : IBase64Encoder {
    abstract val encoder: IJSBase64Adapter

    override fun encode(bytes: ByteArray): ByteArray = encodeToString(bytes).encodeToByteArray()

    override fun encodeToString(bytes: ByteArray): String {
        val u8s = Uint8Array(bytes.toTypedArray())
        return paddingUpdate(encoder.fromUint8Array(u8s, encoding == Encoding.URL))
    }

    override fun encodeString(str: String): String = paddingUpdate(encoder.encode(str, encoding == Encoding.URL))

    private fun paddingUpdate(converted: String): String = when (padding) {
        EncoderImpl.StdPadding -> ensurePadding(converted)
        EncoderImpl.NoPadding -> converted.substringBefore(EncoderImpl.StdPadding)
        else -> converted.replace(EncoderImpl.StdPadding, padding)
    }

    private fun ensurePadding(at: String): String = if (at.length % 4 == 0) at
    else at.plus(padding.toString().repeat(at.length % 4))
}