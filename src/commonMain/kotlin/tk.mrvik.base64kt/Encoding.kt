package tk.mrvik.base64kt

enum class Encoding(val enc: ByteArray) {
    Standard("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".encodeToByteArray()),
    URL("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".encodeToByteArray());

    val reversed = enc.mapIndexed { i, v -> v to i.toByte() }.associate { it }
}