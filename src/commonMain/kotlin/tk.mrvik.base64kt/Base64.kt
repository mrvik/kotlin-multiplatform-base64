package tk.mrvik.base64kt

interface IBase64Encoder {
    fun encode(bytes: ByteArray): ByteArray
    fun encodeToString(bytes: ByteArray): String
    fun encodeString(str: String): String
}

interface IBase64Decoder {
    fun decode(bytes: ByteArray): ByteArray
    fun decodeString(str: String): ByteArray
    fun decodeToString(str: String): String
}

expect object Base64Factory {
    fun createEncoder(encoding: Encoding = Encoding.Standard, padding: Char? = EncoderImpl.StdPadding): IBase64Encoder
    fun createDecoder(encoding: Encoding = Encoding.Standard, padding: Char? = EncoderImpl.StdPadding): IBase64Decoder
}