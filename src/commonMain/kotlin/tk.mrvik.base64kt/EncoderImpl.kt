package tk.mrvik.base64kt

class EncoderImpl(private val encoding: Encoding, private val padding: Char? = StdPadding): IBase64Encoder {
    companion object {
        val NoPadding = null
        const val StdPadding = '='
    }

    override fun encode(bytes: ByteArray): ByteArray {
        if (bytes.isEmpty()) return ByteArray(0)

        val rt = ByteArray(encodedLength(bytes.size))

        var di = 0
        var si = 0
        val n = (bytes.size / 3) * 3

        while (si < n) {
            // Convert 3x 8bit source bytes into 4 bytes
            val value = ((0xff and bytes[si + 0].toInt()) shl 16) +
                    (0xff and (bytes[si + 1].toInt()) shl 8) +
                    (0xff and bytes[si + 2].toInt())
            rt[di + 0] = encoding.enc[(value shr 18 and 0x3f)]
            val index1 = (value shr 12 and 0x3f)
            rt[di + 1] = encoding.enc[index1]
            rt[di + 2] = encoding.enc[(value shr 6 and 0x3f)]
            rt[di + 3] = encoding.enc[(value and 0x3f)]

            si += 3
            di += 4
        }

        val remaining = bytes.size - si

        if (remaining == 0) return rt

        var value = bytes[si + 0].toUInt() shl 16
        if (remaining == 2) {
            value = value or (bytes[si + 1].toUInt() shl 8)
        }

        rt[di + 0] = encoding.enc[(value shr 18 and 0x3fu).toInt()]
        rt[di + 1] = encoding.enc[(value shr 12 and 0x3fu).toInt()]

        if (remaining == 2) {
            rt[di + 2] = encoding.enc[(value shr 6 and 0x3fu).toInt()]
            if (padding != NoPadding) rt[di + 3] = padding.code.toByte()
        } else if (remaining == 1 && padding != NoPadding) {
            rt[di + 2] = padding.code.toByte()
            rt[di + 3] = padding.code.toByte()
        }

        return rt
    }

    override fun encodeString(str: String): String {
        return encodeToString(str.encodeToByteArray())
    }

    override fun encodeToString(bytes: ByteArray): String {
        return encode(bytes).decodeToString()
    }

    fun encodedLength(n: Int): Int {
        return if (padding == NoPadding)
            (n * 8 + 5) / 6
        else (n + 2) / 3 * 4
    }
}