package tk.mrvik.base64kt

import kotlin.math.ceil
import kotlin.math.min

class DecoderImpl(
    private val encoding: Encoding = Encoding.Standard, private val padding: Char? = EncoderImpl.StdPadding
) : IBase64Decoder {
    companion object {
        private const val decodeFromLength = 8
        private const val decodeToLength = 6
    }

    override fun decode(bytes: ByteArray): ByteArray = if (bytes.isEmpty()) ByteArray(0) else
        List(ceil(decodedLen(bytes.size).toDouble() / decodeToLength.toDouble()).toInt()) {
            // Each one of these slices contain up to **8 bytes**
            val start = it * decodeFromLength
            val end = min(start + decodeFromLength, bytes.size)

            bytes.copyOfRange(start, end)
        }.map {
            // Decode each byte group
            var finalLong = 0UL // Unsigned long (64 bits)
            it.let {
                    if (padding == EncoderImpl.NoPadding) it
                    else it.filter { b -> b.toInt().toChar() != padding }.toByteArray()
                }.map { byte ->
                    encoding.reversed[byte]
                        ?: throw IllegalArgumentException("Received bytes don't match selected encoding")
                }  // Get respective byte from encoding
                .forEachIndexed { index, byte ->
                    // Append each decoded byte to finalLong
                    val shift = (ULong.SIZE_BITS - (index * decodeToLength + decodeToLength))
                    finalLong = finalLong or ((0xff and byte.toInt()).toULong() shl shift)
                }

            finalLong
        }.flatMap {
            List(decodeToLength) { index -> uLongMask(it, index).toByte() }
        }.let {
            it.dropLastWhile { b -> b == 0.toByte() }
        }.toByteArray()

    /**
     * An ULong has 8 bytes, so we have to leave the byte specified in byteIndex as the **least** significant.
     * When .toByte() is called over the result, it returns the desired byte.
     */
    private fun uLongMask(from: ULong, byteIndex: Int): ULong =
        (from shr (ULong.SIZE_BITS - ((byteIndex + 1) * 8))) and 0xffUL

    override fun decodeString(str: String): ByteArray = decode(str.encodeToByteArray())

    override fun decodeToString(str: String): String = decodeString(str).decodeToString()

    private fun decodedLen(n: Int): Int = if (padding == EncoderImpl.NoPadding) n * 6 / 8
    else n / 4 * 3
}